import React, { Fragment } from 'react'
import Topbar from '../../components/topbar/Topbar'
import Sidebar from '../../components/sidebar/Sidebar'
import Feed from '../../components/feed/Feed'
import Rightbar from '../../components/rightbar/Rightbar'
import './home.css'
function Home() {
  return (
    <Fragment>
      <Topbar />
      <div className='homecontainer'>
        <Sidebar />
        <Feed />
        <Rightbar />
      </div>
    </Fragment>
  )
}

export default Home
