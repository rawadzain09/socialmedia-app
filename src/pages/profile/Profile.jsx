import React, { Fragment, useState, useEffect } from 'react'
import './profile.css'
import Topbar from '../../components/topbar/Topbar'
import Sidebar from '../../components/sidebar/Sidebar'
import Feed from '../../components/feed/Feed'
import Rightbar from '../../components/rightbar/Rightbar'
import axios from 'axios'
import { useParams } from 'react-router-dom'
function Profile({ post }) {
  const PF = process.env.REACT_APP_PUBLIC_FOLDER
  const username = useParams().username
  const [user, setUser] = useState({})
  useEffect(() => {
    var config = {
      headers: {
        'Content-Type': 'application/json',
      },
    }

    const fetchUser = async () => {
      try {
        const res = await axios.get(`/users/${user._id}`, {
          config,
          responseType: 'json',
        })
        setUser(res.data)
      } catch (error) {
        console.log(error)
      }
    }

    fetchUser()
  }, [user._id])

  return (
    <Fragment>
      <Topbar />
      <div className='profile'>
        <Sidebar />
        <div className='profileRight'>
          <div className='profileRightTop'>
            <div className='profileCover'>
              <img
                src={`${PF}post/3.jpeg`}
                alt=''
                className='profileCoverImg'
              />
              <img
                src={`${PF}person/7.jpeg`}
                alt=''
                className='profileUSerImg'
              />
            </div>
            <div className='profileInfo'>
              <h4 className='profileInfoName'>{user.username}</h4>
              <span className='profileInfoDesc'> {user.desc}</span>
            </div>
          </div>
          <div className='profileRightBottom'>
            <Feed username={'john'} />
            <Rightbar user={user} />
          </div>
        </div>
      </div>
    </Fragment>
  )
}

export default Profile
