import React from 'react'
import './register.css'

function Register() {
  return (
    <div className='login'>
      <div className='loginWrapper'>
        <div className='loginLeft'>
          <h3 className='loginLogo'>LamaSocial</h3>
          <span className='loginDesc'>
            Connect with Friends and the world around you on LamaSocail
          </span>
        </div>
        <div className='loginRight'>
          <div className='loginBox'>
            <input placeholder='Username' className='loginInput' />
            <input placeholder='Email' className='loginInput' />
            <input placeholder='Password' className='loginInput' />
            <input placeholder='Password Again' className='loginInput' />
            <button className='loginButton'>Sign up</button>
          </div>
          <button className='loginRegisterButton'>Log into Account</button>
        </div>
      </div>
    </div>
  )
}

export default Register
