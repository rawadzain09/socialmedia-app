import React from 'react'
import './online.css'

function OnLine({ user }) {
  const PF = process.env.REACT_APP_PUBLIC_FOLDER
  return (
    <li className='rightbarFriend'>
      <div className='rightbarProfileImgContainer'>
        <img
          src={PF + user.profilePicture}
          alt=''
          className='rightbarProfileImg'
        />
        <span className='rightbarOnLine'></span>
      </div>
      <span className='rightbarUsername'>{user.username} </span>
    </li>
  )
}

export default OnLine
