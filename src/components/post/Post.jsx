import React, { useState, useEffect } from 'react'
import './post.css'
import { MoreVert } from '@mui/icons-material'
import axios from 'axios'
import { Link } from 'react-router-dom'
import { format } from 'timeago.js'

function Post({ post }) {
  const PF = process.env.REACT_APP_PUBLIC_FOLDER
  const [like, setLike] = useState(post.likes.length)
  const [isliked, setISLikeD] = useState(false)
  const LikedHandeler = () => {
    setLike(isliked ? like - 1 : like + 1)
    setISLikeD(!isliked)
  }
  const [user, setUser] = useState({})
  useEffect(() => {
    var config = {
      headers: {
        'Content-Type': 'application/json',
      },
    }
    const fetcUser = async () => {
      const res = await axios.get(`/users/${post.userId}`, { config })
      setUser(res.data)
    }

    fetcUser()
  }, [post.userId])

  return (
    <div className='post'>
      <div className='postWrapper'>
        <div className='postTop'>
          <div className='postTopLeft'>
            <Link to={`/profile/${user.username}`}>
              <img
                className='postProfileImg'
                src={user.profilePicture || PF + 'person/noAvatar.png'}
                alt=''
              />
            </Link>
            <span className='postUsername'>{user.username}</span>
            <span className='postUsername'>{format(post.createdAt)}</span>
          </div>
          <div className='postTopRight'>
            <MoreVert />
          </div>
        </div>
        <div className='postCenter'>
          <span className='postText'>{post.desc}</span>
          <img className='postImg' src={PF + post.img} alt='' />
        </div>
        <div className='postBottom'>
          <div className='postBottomLeft'>
            <img
              className='likeIcon'
              src={`${PF}like.png `}
              alt=''
              onClick={LikedHandeler}
            />
            <img className='likeIcon' src={`${PF}heart.png `} alt='' />
            <span className='postLikeCounter'>{like} people like it</span>
          </div>
          <div className='postBottomRight'>
            <span className='postCommentText'>{post.comment} comments</span>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Post
