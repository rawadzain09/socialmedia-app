import React, { useEffect, useState } from 'react'
import './feed.css'
import Share from '../share/Share'
import Post from '../post/Post'
import axios from 'axios'

function Feed({ username }) {
  const [posts, setPost] = useState([])

  useEffect(
    (pr) => {
      const fetcPosts = async () => {
        const res = username
          ? await axios.get(`/posts/profile/${username} `)
          : await axios.get('/posts/timeline/64d8ef61478e4a43e4767a89')
        setPost(res.data)
      }
      fetcPosts()
    },
    [username]
  )

  return (
    <div className='feed'>
      <div className='feedWrapper'>
        <Share />
        {posts.map((p) => (
          <Post key={p._id} post={p} />
        ))}
      </div>
    </div>
  )
}

export default Feed
