import React, { Fragment } from 'react'
import './rightbar.css'
import { Users } from '../dummyData'
import OnLine from '../onLine/OnLine'

function Rightbar({ user }) {
  const PF = process.env.REACT_APP_PUBLIC_FOLDER
  const HomeRightBar = () => {
    return (
      <Fragment>
        <div className='birthdayContainer'>
          <img className='birthdayImg' src='/assets/gift.png' alt='' />
          <span className='birthdayText'>
            <b> Pola Foster</b> and <b>3 friends have a birthday today.</b>
          </span>
        </div>
        <img src='/assets/ad.png' alt='' className='rightbarAd' />
        <h4 className='rightbarTitle'>OnLine Friends</h4>
        <ul className='rightbarFriendList'>
          {Users.map((u) => (
            <OnLine key={u.id} user={u} />
          ))}
        </ul>
      </Fragment>
    )
  }
  const ProfileRightBar = () => {
    return (
      <Fragment>
        <h4 className='rightbarTitle'> User information</h4>
        <div className='rightbarInfo'>
          <div className='rightbarInfoItem'>
            <span className='rightbarInfoKey'>City:</span>
            <span className='rightbarInfoValue'>{user.city}</span>
          </div>
          <div className='rightbarInfoItem'>
            <span className='rightbarInfoKey'>From:</span>
            <span className='rightbarInfoValue'>{user.from}</span>
          </div>
          <div className='rightbarInfoItem'>
            <span className='rightbarInfoKey'>Relationship</span>
            <span className='rightbarInfoValue'>{user.relationship}</span>
          </div>
        </div>
        <h4 className='rightbarTitle'>User Friends</h4>
        <div className='rightbarFollowings'>
          <div className='rightbarFollowing'>
            <img
              src={`${PF}person/1.jpeg `}
              alt=''
              className='rightbarFollowingImg'
            />
            <span className='rightbarFollowingName'>John Cartner</span>
          </div>
          <div className='rightbarFollowing'>
            <img
              src={`${PF}person/2.jpeg `}
              alt=''
              className='rightbarFollowingImg'
            />
            <span className='rightbarFollowingName'>John Cartner</span>
          </div>
          <div className='rightbarFollowing'>
            <img
              src={`${PF}person/3.jpeg `}
              alt=''
              className='rightbarFollowingImg'
            />
            <span className='rightbarFollowingName'>John Cartner</span>
          </div>
          <div className='rightbarFollowing'>
            <img
              src={`${PF}person/4.jpeg `}
              alt=''
              className='rightbarFollowingImg'
            />
            <span className='rightbarFollowingName'>John Cartner</span>
          </div>
          <div className='rightbarFollowing'>
            <img
              src={`${PF}person/5.jpeg `}
              alt=''
              className='rightbarFollowingImg'
            />
            <span className='rightbarFollowingName'>John Cartner</span>
          </div>
          <div className='rightbarFollowing'>
            <img
              src={`${PF}person/6.jpeg `}
              alt=''
              className='rightbarFollowingImg'
            />
            <span className='rightbarFollowingName'>John Cartner</span>
          </div>
        </div>
      </Fragment>
    )
  }
  return (
    <div className='rightbar'>
      <div className='RighrbarWrapper'>
        {user ? <ProfileRightBar /> : <HomeRightBar />}
      </div>
    </div>
  )
}

export default Rightbar
