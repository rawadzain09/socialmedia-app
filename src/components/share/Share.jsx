import React from 'react'
import './share.css'
import { PermMedia, Label, Room, EmojiEmotions } from '@mui/icons-material'

function Share() {
  return (
    <div className='share'>
      <div className='shareWrapper'>
        <div className='shareTop'>
          <img src='/assets/person/1.jpeg' alt='' className='shareProfileImg' />
          <input
            placeholder="what's in your mind Safak?"
            className='shareInput'
          />
          <hr className='shareHr' />
        </div>
        <div className='shareBottom'>
          <div className='shareOptions'>
            <div className='shareOption'>
              <PermMedia htmlColor='tomato' className='shareMedia' />
              <span className='shareOptionText'>Photo Or Video</span>
            </div>
            <div className='shareOption'>
              <Label className='shareMedia' htmlColor='blue' />
              <span className='shareOptionText'>Tag</span>
            </div>
            <div className='shareOption'>
              <Room className='shareMedia' htmlColor='green' />
              <span className='shareOptionText'>Location</span>
            </div>
            <div className='shareOption'>
              <EmojiEmotions className='shareMedia' htmlColor='goldenrod' />
              <span className='shareOptionText'>Feelings</span>
            </div>
          </div>
          <button className='shareButton'>Share</button>
        </div>
      </div>
    </div>
  )
}

export default Share
